public class Fibonacci {

        // recursive
        public static long fibonacci(long num) {
            if ((num == 0) || (num == 1)) // base cases
                return num;
            else
                // recursion step
                return fibonacci(num - 1) + fibonacci(num - 2);
        }

        public static void main(String[] args) {


            long start1 = System.nanoTime();
            int num = 30;
            long answer[] = new long[num];
            for (int i = 0; i < num; i++) {
                answer[i] = fibonacci(i+1);
            }

            // print the table of the answers.
            for (int i = 0; i <  num; i++)
                System.out.printf("Fibonacci of %d is: %d\n", i+1, answer[i]);
            long end1 = System.nanoTime();


            long start2 = System.nanoTime();
            fiboSimple();
            long end2 = System.nanoTime();

            System.out.println("Time of recursion (millisec) " +((end1 - start1) / 1000000));
            System.out.println("Time of no recursion (millisec) " +((end2 - start2) / 1000000));

        }



        public static void fiboSimple(){
            int n1=0,n2=1,n3=0,count=30;
            System.out.print(n2+" ");
            for (int i=2;i<=count;i++){
                n3 = n1+n2;
                n1=n2;
                n2=n3;
                System.out.print(" "+n3);
            }
        }
    }

