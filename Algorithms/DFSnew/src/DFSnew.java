import java.util.Iterator;
import java.util.LinkedList;

public class DFSnew {

    private int Vertex;
    private LinkedList<Integer> adjacent[];

    public DFSnew(int vertex){
        Vertex = vertex;
        adjacent = new LinkedList[vertex];
        for (int i=0; i<vertex; i++){
            adjacent[i] = new LinkedList();
        }
    }

    public static void main(String[] args){
        DFSnew graph = new DFSnew(4);

        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);
        System.out.println("Following is Depth First  "+ "(starting from vertex 2)");
        graph.DFS(2);

    }


    public void addEdge(int vertex,int num){
        adjacent[vertex].add(num);
    }

    public void checkVisited(int vertex,boolean visited[]){
        visited[vertex] = true;
        System.out.print(vertex+" ");
        // Recur for all the vertices adjacent to this vertex
        Iterator<Integer> i = adjacent[vertex].listIterator();

        while (i.hasNext()) {
            int n = i.next();
            if (!visited[n])
                checkVisited(n, visited);
        }
    }

    public void DFS(int vertex){
            // Mark all the vertices as not visited(set as
            // false by default in java)
            boolean visited[] = new boolean[Vertex];
            // Call the recursive helper function to print DFS traversal
        checkVisited(vertex, visited);
    }

}
