import java.util.Iterator;
import java.util.LinkedList;

public class BFSnew {

    private int Vertex;   // No. of vertices
    private LinkedList<Integer> adjacent[]; //Adjacency Lists

    // Constructor
    public BFSnew(int vertex)
    {
        Vertex = vertex;
        adjacent = new LinkedList[vertex];
        for (int i=0; i<vertex; ++i)
            adjacent[i] = new LinkedList();
    }

    // Function to add an edge into the graph
    public void addEdge(int vertex,int num)
    {
        adjacent[vertex].add(num);
    }

    // prints BFS traversal from a given source s
    public void BFS(int num)
    {
        // Mark all the vertices as not visited(By default
        // set as false)
        boolean visited[] = new boolean[Vertex];

        // Create a queue for BFS
        LinkedList<Integer> queue = new LinkedList<Integer>();

        // Mark the current node as visited and enqueue it
        visited[num]=true;
        queue.add(num);

        while (queue.size() != 0)
        {
            // Dequeue a vertex from queue and print it
            num = queue.poll();
            System.out.print(num+" ");

            // Get all adjacent vertices of the dequeued vertex s
            // If a adjacent has not been visited, then mark it
            // visited and enqueue it
            Iterator<Integer> i = adjacent[num].listIterator();
            while (i.hasNext())
            {
                int n = i.next();
                if (!visited[n])
                {
                    visited[n] = true;
                    queue.add(n);
                }
            }
        }
    }

    // Driver method to
    public static void main(String args[])
    {
        BFSnew graph = new BFSnew(4);

        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);

        System.out.println("Following is Breadth First Traversal "+
                "(starting from vertex 2)");
        graph.BFS(2);
    }
}
