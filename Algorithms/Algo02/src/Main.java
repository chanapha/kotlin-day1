import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int rows = 4;
        int cols = 4;
        int i,j,k=0;
        String[][] multi = new String[rows][cols];

        multi[0][0]="#";
        multi[0][1]=" ";
        multi[0][2]=" ";
        multi[0][3]=" ";

        multi[1][0]="#";
        multi[1][1]=" ";
        multi[1][2]=" ";
        multi[1][3]=" ";

        multi[2][0]="#";
        multi[2][1]=" ";
        multi[2][2]=" ";
        multi[2][3]=" ";

        multi[3][0]="#";
        multi[3][1]="#";
        multi[3][2]="#";
        multi[3][3]="#";


        for(i=0;i<4;i++) {
            for(j=0;j<4;j++) {
                System.out.print(multi[i][j]);
                System.out.print("\t");
            }
            System.out.print("\n");
        }

        System.out.print("\n");
        System.out.println("90 degree");
        for(i=3;i>=0;i--) {
            for(k=0;k<cols;k++) {
                System.out.print(multi[i][k]);
                System.out.print("\t");
            }
            System.out.print("\n");
        }


        System.out.print("\n");
        System.out.println("180 degree");
        for(i=3;i>=0;i--) {
            for(k=3;k>=0;k--) {
                System.out.print(multi[i][k]);
                System.out.print("\t");
            }
            System.out.print("\n");
        }

        System.out.print("\n");
        System.out.println("270 degree");
        for(i=0;i<rows;i++) {
            for(k=3;k>=0;k--) {
                System.out.print(multi[i][k]);
                System.out.print("\t");
            }
            System.out.print("\n");
        }

        /*System.out.println(multi[0][0]+" "+multi[0][1]+" "+multi[0][2]+" "+multi[0][3]);
        System.out.println(multi[1][0]);
        System.out.println(multi[2][0]);
        System.out.println(multi[3][0]);
            // 5
*/

    }

}
